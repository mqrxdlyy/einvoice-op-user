package main

import (
	"flag"

	"einvoice/go/common/app"
	"einvoice/go/common/log"
	"einvoice/go/common/util"
	"einvoice/go/common/web"
	"einvoice/go/op-user/opu"
	userRest "einvoice/go/op-user/rest"
	"github.com/gin-gonic/gin"
	"github.com/json-iterator/go/extra"
)

func main() {
	opu.Api.Mount(opu.Service)
	app.RegisterStarter(opu.Api)
	if err := app.Start(); err != nil {
		panic(err)
	}

	flag.Parse()
	log.Slog.Info("starting rest")
	router := gin.Default()
	router.Use(web.CorsHandler(opu.Api))
	//router.Use(session.InitSessionStore(db.Redis))
	userRest.RegisterAPIs(router)
	extra.SetNamingStrategy(util.LowerFirst)

	log.Slog.Fatal(router.Run(":" + opu.Api.Port))
	log.Slog.Info("rest started")
}
