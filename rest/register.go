package rest

import (
	"einvoice/go/op-user/rest/handler"
	"github.com/gin-gonic/gin"
)

func RegisterAPIs(router gin.IRouter) {
	handler.NewUserAPI().Register(router)
	handler.NewRoleAPI().Register(router)
	handler.NewGroupAPI().Register(router)
}

func RegisterAPIsWithLogInterceptor(router gin.IRouter) {
	RegisterAPIs(router)
}
