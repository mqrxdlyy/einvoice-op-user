package service

import (
	"fmt"

	"einvoice/go/common"
	app "einvoice/go/common/app"
	"einvoice/go/common/log"
	ps "einvoice/go/common/password"
	"einvoice/go/op-user/model"
	"einvoice/go/op-user/opu"
	"github.com/go-ldap/ldap/v3"
	"github.com/spf13/viper"
	"go.uber.org/zap"
)

type Authenticator interface {
	authenticate(username string, password string) (success bool, err error)
	build(*viper.Viper)
	Enabled() bool
}

type baseAuthenenticator struct {
	enabled bool
}

func (authenticator *baseAuthenenticator) Enabled() bool {
	return authenticator.enabled
}

var authenticators map[string]Authenticator

type dbAuthenticator struct {
	baseAuthenenticator
}

func (authenticator *dbAuthenticator) authenticate(username string, password string) (success bool, err error) {
	success = true
	if password == "" {
		success = false
		err = fmt.Errorf(model.AUTH_PASSWORD_INVALID)
		log.Logger.Error("password not match")
		return
	}
	user := &model.User{}
	has, err := User.Db.Where(" dtd = false").And("mobile = ? or email ilike ? or username ilike ?", username, username, username).Get(user)
	log.Logger.Debug("select user by mobile", zap.Bool("has", has))
	if err != nil {
		log.Logger.Error("failed to get user by mobile", zap.Any("err", err))
		return
	}

	if user.Id <= 0 {
		success = false
		err = fmt.Errorf(model.LOGIN_FAILED)
		return
	}

	if !ps.Validate(password, user.Password) || password == "" {
		success = false
		err = fmt.Errorf(model.AUTH_PASSWORD_INVALID)
		log.Logger.Error("password not match")
		return
	}
	return
}

func (authenticator *dbAuthenticator) build(subConf *viper.Viper) {
	authenticator.baseAuthenenticator.enabled = subConf.GetBool("enabled")
}

type ldapAuthenticator struct {
	baseAuthenenticator
	Uri string
	Dn  string
}

func (authenticator *ldapAuthenticator) authenticate(username string, password string) (success bool, err error) {
	success = true
	l, err := ldap.DialURL(authenticator.Uri)
	if err != nil {
		success = false
		return
	}
	defer l.Close()

	err = l.Bind(username+"@"+authenticator.Dn, password)
	if err != nil {
		success = false
		log.Logger.Error("username or password not match", zap.Error(err))
		return
	}
	return
}

func (authenticator *ldapAuthenticator) build(subConf *viper.Viper) {
	authenticator.baseAuthenenticator.enabled = subConf.GetBool("enabled")
	authenticator.Uri = subConf.GetString("uri")
	authenticator.Dn = subConf.GetString("dn")
}

type baseAuthenticatorConfig struct {
	Enabled bool
}

func init() {
	authenticators = map[string]Authenticator{
		"db":   &dbAuthenticator{baseAuthenenticator: baseAuthenenticator{enabled: true}},
		"ldap": &ldapAuthenticator{},
	}
	// opu.Service.Subscribe("authenticator.ldap", &ldapAuthenticator)
	app.OnStarted(opu.Service.Name(), func(ctx common.Context) error {
		var baseConfs map[string]baseAuthenticatorConfig
		err := opu.Service.RawConfig.UnmarshalKey("org", &OrgEnabled)
		if err != nil {
			log.Logger.Error("not found org config. ", zap.Error(err))
			OrgEnabled.Enabled = true
		}
		log.Logger.Debug("org config. ", zap.Bool("Org", OrgEnabled.Enabled))
		err = opu.Service.RawConfig.UnmarshalKey("authenticator", &baseConfs)
		if err != nil {
			return err
		}

		for key, conf := range baseConfs {
			if !conf.Enabled {
				continue
			}

			if authenticators[key] == nil {
				log.Logger.Warn("unsupported authenticator " + key)
				continue
			}

			authenticators[key].build(opu.Service.RawConfig.Sub("authenticator." + key))
		}

		return nil
	})
}
